# Use the official image as a parent image.
FROM osgeo/gdal:latest

COPY . /temp
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update -y -qq && apt-get install -y tzdata curl nano && apt-get install software-properties-common -y -qq && add-apt-repository ppa:ubuntugis/ppa -y && add-apt-repository ppa:deadsnakes/ppa -y
# installing fftw & python 3.9
RUN apt-get update -y -qq && apt-get upgrade -y -qq && apt-get install -y -q libfftw3-dev swig nodejs python3.9 python3.9-venv python3.9-dev python3-pip && apt-get clean

# make python 3.9 the default version
RUN update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.9 1 && update-alternatives --install /usr/bin/python python /usr/bin/python3.9 1

ARG CPLUS_INCLUDE_PATH=/usr/include/gdal
ARG C_INCLUDE_PATH=/usr/include/gdal

## install python packages
RUN pip3 install --upgrade -r /temp/docker-requirements.txt

# install gdal
RUN apt-get update -y -qq && apt-get install -y gdal-bin python3-gdal libgdal-dev && pip3 install GDAL==$(gdal-config --version)

RUN rm -r /temp

## setup jupyter Lab
RUN pip3 install ipywidgets && jupyter nbextension enable --py widgetsnbextension
EXPOSE 8888
RUN export JUPYTERLAB_DIR="$HOME/.local/share/jupyter/lab" && jupyter nbextension enable --py widgetsnbextension
